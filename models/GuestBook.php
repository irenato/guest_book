<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guest_book".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $email
 * @property string $homepage
 * @property string $text
 * @property string $ip
 * @property integer $created_at
 */
class GuestBook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guest_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'email', 'text', 'ip', 'created_at'], 'required'],
            [['text'], 'string'],
            [['created_at'], 'integer'],
            [['user_name'], 'string', 'max' => 64],
            [['email', 'homepage'], 'string', 'max' => 128],
            [['ip'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_name' => 'User Name',
            'email' => 'Email',
            'homepage' => 'Homepage',
            'text' => 'Text',
            'ip' => 'Ip',
            'created_at' => 'Created At',
        ];
    }
    public function addNewNotation($model)
    {
        $this->user_name = $model->user_name;
        $this->email = $model->email;
        if (!empty($model->homepage)) {
            $this->homepage = $model->homepage;
        }
        $this->text = $model->text;
        $this->ip = Yii::$app->request->userIP;
        $this->created_at = time();
        $this->save();
    }
}
