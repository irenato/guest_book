$(document).ready(function () {
    $("select.form-control").change(function () {
        var column = $(this).val(),
            send_data = $.ajax({
                data: {
                    'column': column,
                },
                url: 'site/sort-param',
                type: 'post',
                dataType: 'html',
            });
        send_data.done(function () {
            location.reload();
        })
    })

    $("input#inlineCheckbox1").change(function () {
        if ($(this).prop("checked") == true) {
            var descending = 'DESC';
        } else {
            var descending = '';
        }
        var send_data = $.ajax({
            data: {
                'descending': descending,
            },
            url: 'site/sort-param',
            type: 'post',
            dataType: 'html',
        });
        send_data.done(function () {
            location.reload();
        })
    })
})

