<?php
use yii\widgets\LinkPager;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */

$this->title = 'GuestBook';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome to GuestBook!</h1>
        <p><a class="btn btn-lg btn-success" href="<?= Yii::$app->urlManager->createUrl(['site/add']) ?>">Add
                new notation</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <h2>All Notations</h2>
            <div class="col-lg-offset-10">
                <span>sort By</span>
                <select class="form-control">
                    <option value="created_at" <?php if ($column_name == 'created_at') echo 'selected' ?>>Date</option>
                    <option value="user_name" <?php if ($column_name == 'user_name') echo 'selected' ?>>Name
                    </option>
                    <option value="email" <?php if ($column_name == 'email') echo 'selected' ?>>Email
                    </option>
                </select>
            </div>
            <div class="col-lg-offset-10">
                <label class="checkbox-inline">
                    <input type="checkbox"
                           id="inlineCheckbox1" <?php if (!empty($_SESSION['descending'])) echo 'checked'; ?> >
                    descending
                </label>
            </div>
            <table class="table table-bordered">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Homepage</th>
                    <th>Text</th>
                    <th>IP</th>
                    <th>Date</th>
                </tr>
                <?php foreach ($notations as $notation) : ?>
                    <tr>
                        <td><?= $notation['id'] ?></td>
                        <td><?= $notation['user_name'] ?></td>
                        <td><?= $notation['email'] ?></td>
                        <td><?= $notation['homepage'] ?></td>
                        <td><?= $notation['text'] ?></td>
                        <td><?= $notation['ip'] ?></td>
                        <td><?= date('d-m-Y H-i-s', $notation['created_at']) ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
            <div class="clearfix"></div>
            <div class="col-md-8"></div>
            <?= LinkPager::widget(['pagination' => $pagination]); ?>
        </div>

    </div>

</div>


