<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\GuestBook;
use app\models\GuestBookForm;
use yii\data\Pagination;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex()
    {
        if (!isset($_SESSION))
            session_start();
        $column_name = isset($_SESSION['column']) ? $_SESSION['column'] : 'created_at';
        $descending = isset($_SESSION['descending']) ? $_SESSION['descending'] : 'DESC';
        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => GuestBook::find()->count(),
        ]);
        $notations = GuestBook::find()
            ->orderBy($column_name . ' ' . $descending)
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        return $this->render('index', [
            'notations' => $notations,
            'pagination' => $pagination,
            'column_name' => $column_name,
            'descending' => $descending,
        ]);
    }

    public function actionAdd()
    {
        $model = new GuestBookForm();
        $notation = new GuestBook();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $notation->addNewNotation($model);

            return $this->goBack();
        }
        return $this->render('add', [
            'model' => $model,
        ]);

    }

    public function actionSortParam()
    {
        if (!isset($_SESSION))
            session_start();
        if (isset($_POST['column'])) {
            $_SESSION['column'] = $_POST['column'];
        } elseif (isset($_POST['descending'])) {
            $_SESSION['descending'] = $_POST['descending'];
        }
    }

}
